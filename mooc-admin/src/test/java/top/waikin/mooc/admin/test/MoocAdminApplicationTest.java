package top.waikin.mooc.admin.test;


import cn.hutool.core.collection.CollUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import top.waikin.mooc.admin.service.RoleService;
import top.waikin.mooc.common.service.RedisService;
import top.waikin.mooc.entity.Course;

import java.util.ArrayList;
import java.util.Collections;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MoocAdminApplicationTest {
    @Autowired
    RoleService roleService;

    @Autowired
    RedisService redisService;

    @Test
    public void test() {
        redisService.lPush("testList", CollUtil.toList(new Course().setId(1), new Course().setId(2)));
    }
}
