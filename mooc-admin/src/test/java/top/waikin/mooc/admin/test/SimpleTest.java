package top.waikin.mooc.admin.test;

import cn.hutool.core.collection.CollectionUtil;
import org.assertj.core.util.Arrays;
import org.junit.Test;
import top.waikin.mooc.entity.Role;

import java.util.List;
import java.util.stream.Collectors;

public class SimpleTest {
    @Test
    public void test(){
        List<Integer> roleIds = CollectionUtil.newArrayList(4,10);
        List<Integer> oldRoleIds = CollectionUtil.newArrayList(1,2,3,4,5);

        List<Integer> reduceIds = CollectionUtil.subtractToList(oldRoleIds, roleIds);
        List<Integer> increaseIds = CollectionUtil.subtractToList(roleIds, oldRoleIds);

        System.out.println(reduceIds);
        System.out.println(increaseIds);
    }
}
