package top.waikin.mooc.admin.service;

import top.waikin.mooc.admin.domain.CarouselDto;
import top.waikin.mooc.entity.Carousel;
import top.waikin.mooc.entity.Course;

import java.util.List;

/**
 * 主页服务类
 */
public interface HomeCacheService {

    void createRecommendCourse(Integer courseId);

    void deleteRecommendCourse(Integer courseId);

    List<Course> getRecommendCourseList();

    int createCarousel(CarouselDto carouselDto);

    void deleteCarousel(Integer carouselIndex);

    void updateCarousel(Integer carouselIndex, CarouselDto carouselDto);

    List<Carousel> getCarouselList();
}
