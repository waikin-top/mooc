package top.waikin.mooc.admin.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.LocalDateTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;
import top.waikin.mooc.admin.service.CourseService;
import top.waikin.mooc.admin.service.RankCacheService;
import top.waikin.mooc.common.exception.Asserts;
import top.waikin.mooc.entity.Course;
import top.waikin.mooc.entity.CourseRankTuple;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class RankCacheServiceImpl implements RankCacheService {

    @Autowired
    RedisTemplate<String, Object> redisTemplate;

    @Autowired
    CourseService courseService;

    @Value("${redis.database}")
    private String REDIS_DATABASE;

    @Value("${redis.key.courseDayRank}")
    private String COURSE_DAY_RANK;

    @Value("${redis.key.courseWeekRank}")
    private String COURSE_WEEK_RANK;

    @Override
    public List<CourseRankTuple> getRankCourseList(String type, LocalDate localDate) {
        switch (type) {
            case "day":
                return getDayRankCourseList(localDate);
            case "week":
                return getWeekRankCourseList(localDate);
            default:
                return null;
        }
    }

    private List<CourseRankTuple> getDayRankCourseList(LocalDate localDate) {
        String key = REDIS_DATABASE + ":" + COURSE_DAY_RANK + ":" + LocalDateTimeUtil.formatNormal(localDate);
        return redisTemplate.opsForZSet().reverseRangeWithScores(key, 0, 49)
                .stream().map(objectTypedTuple -> new CourseRankTuple().setCourse(courseService.getById(objectTypedTuple.getValue().toString())).setScore(objectTypedTuple.getScore())).collect(Collectors.toList());
    }

    private List<CourseRankTuple> getWeekRankCourseList(LocalDate localDate) {
        LocalDate yesterday = LocalDate.now().plusDays(-1);
        if (localDate.isAfter(yesterday)) {
            Asserts.fail("周排名时间不能超过昨天");
        }
        String key = REDIS_DATABASE + ":" + COURSE_WEEK_RANK + ":" + LocalDateTimeUtil.formatNormal(localDate);
        Set<ZSetOperations.TypedTuple<Object>> typedTuples = redisTemplate.opsForZSet().reverseRangeWithScores(key, 0, -1);
        if (CollUtil.isEmpty(typedTuples)) {
            // 为空则生成排行
            generateWeekRank(localDate);
            typedTuples = redisTemplate.opsForZSet().reverseRangeWithScores(key, 0, -1);
        }
        return typedTuples.stream().map(objectTypedTuple -> new CourseRankTuple().setCourse((Course) objectTypedTuple.getValue()).setScore(objectTypedTuple.getScore())).collect(Collectors.toList());
    }

    public void generateWeekRank(LocalDate localDate) {
        Date today = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        // 收集时间从昨天到7天前
        Date end = DateUtil.offsetDay(today, -1);
        DateTime start = DateUtil.offsetDay(today, -7);
        String key = REDIS_DATABASE + ":" + COURSE_WEEK_RANK + ":" + DateUtil.formatDate(today);
        String day_key_prefix = REDIS_DATABASE + ":" + COURSE_DAY_RANK + ":";
        List<String> dayKeyList = DateUtil.rangeToList(start, end, DateField.DAY_OF_WEEK)
                .stream().map(DateUtil::formatDate).map(s -> day_key_prefix + s).collect(Collectors.toList());
        System.out.println(dayKeyList);
        // 获取7天的联合
        Set<ZSetOperations.TypedTuple<Object>> courseIdTypedTuples = redisTemplate.opsForZSet().unionWithScores("", dayKeyList);
        // 再存储前20
        Set<ZSetOperations.TypedTuple<Object>> save = courseIdTypedTuples.stream().sorted().limit(20)
                .map(objectTypedTuple -> ZSetOperations.TypedTuple.of((Object) courseService.getById(objectTypedTuple.getValue().toString()), objectTypedTuple.getScore())).collect(Collectors.toSet());
        redisTemplate.opsForZSet().add(key, save);
    }
}
