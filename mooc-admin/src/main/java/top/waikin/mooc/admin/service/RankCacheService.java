package top.waikin.mooc.admin.service;

import top.waikin.mooc.entity.CourseRankTuple;

import java.time.LocalDate;
import java.util.List;

public interface RankCacheService {
    List<CourseRankTuple> getRankCourseList(String type, LocalDate localDate);
}
