package top.waikin.mooc.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.waikin.mooc.entity.RolePermissionRelation;

import java.util.List;

/**
 * 后台 角色和资源关系 服务类
 */
public interface RolePermissionRelationService extends IService<RolePermissionRelation> {
    List<Integer> getAdminIdListByPermissionId(Integer permissionId);
}
