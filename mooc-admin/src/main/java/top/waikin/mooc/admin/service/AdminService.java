package top.waikin.mooc.admin.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.transaction.annotation.Transactional;
import top.waikin.mooc.admin.domain.AdminEditDto;
import top.waikin.mooc.admin.domain.AdminInfoDto;
import top.waikin.mooc.entity.Admin;
import top.waikin.mooc.entity.Permission;
import top.waikin.mooc.entity.Role;

import java.util.List;


/**
 * 管理员 服务类
 */
public interface AdminService extends IService<Admin> {
    /**
     * 登录
     */
    void login(String telephone, String password);

    /**
     * 验证码登录
     */
    void loginByAuthCode(String telephone, String authCode);

    /**
     * 注册
     */
    void register(String telephone, String password, String authCode);

    /**
     * 获取验证码
     */
    void generateAuthCode(String telephone);

    /**
     * 更新密码
     */
    void updatePassword(String telephone, String password, String authCode);

    /**
     * 更新用户信息
     */
    void updateAdminInfo(AdminInfoDto adminInfoDto);

    /**
     * 获取当前用户
     */
    Admin getCurrentAdmin();

    /**
     * 手机号码获取管理员
     */
    Admin getByTelephone(String telephone);

    boolean update(Integer id, AdminEditDto adminEditDto);

    IPage<Admin> page(Integer current, Integer size, String keyWord);


    @Transactional
    boolean updateRole(Integer id, List<Integer> roleIds);

    List<Role> getRoleList(Integer id);

    List<Permission> getPermissionList(Integer id);
}
