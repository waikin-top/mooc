package top.waikin.mooc.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.transaction.annotation.Transactional;
import top.waikin.mooc.admin.domain.RoleDto;
import top.waikin.mooc.entity.Role;

import java.util.List;

/**
 * 后台用户角色 服务类
 */
public interface RoleService extends IService<Role> {

    int create(RoleDto roleDto);

    boolean update(int id, RoleDto roleDto);

    void increaseAdminCount(List<Integer> ids);

    void decreaseAdminCount(List<Integer> ids);


    @Transactional
    boolean updatePermission(Integer id, List<Integer> permissionIds);
}
