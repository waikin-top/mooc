package top.waikin.mooc.admin.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

/**
 * 修改管理员参数
 */
@Data
public class AdminEditDto {
    @NotBlank(message = "昵称不能为空")
    @Length(min = 3, max = 14, message = "昵称字符数为3-14位")
    @ApiModelProperty("昵称")
    private String nickname;

    @URL(message = "头像链接无效")
    @ApiModelProperty("头像")
    private String profile;

    @ApiModelProperty("备注")
    private String note;

    @Min(value = 0,message = "账号启用状态只能为0或1")
    @Max(value = 1,message = "账号启用状态只能为0或1")
    @ApiModelProperty("帐号启用状态:0->禁用,1->启用")
    private Integer status;
}
