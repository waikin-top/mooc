package top.waikin.mooc.admin.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.waikin.mooc.admin.service.RankCacheService;
import top.waikin.mooc.common.api.CommonResult;
import top.waikin.mooc.entity.CourseRankTuple;

import java.time.LocalDate;
import java.util.List;

/**
 * 排行 控制器
 */
@Api(tags = "RankController", description = "排行 控制器")
@RestController
@RequestMapping("/rank")
public class RankController {
    @Autowired
    RankCacheService rankCacheService;

    @ApiOperation("获取指定日期的日或周排行列表")
    @GetMapping("/{type:day|week}/{date:\\d{4}-\\d{2}-\\d{2}}")
    public CommonResult<List<CourseRankTuple>> getCourseRank(@PathVariable String type,
                                                             @PathVariable LocalDate date) {
        List<CourseRankTuple> rankCourseList = rankCacheService.getRankCourseList(type, date);
        return CommonResult.success(rankCourseList);
    }
}
