package top.waikin.mooc.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "top.waikin.mooc")
public class MoocAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(MoocAdminApplication.class, args);
    }
}
