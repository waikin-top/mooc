package top.waikin.mooc.admin.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.waikin.mooc.admin.domain.PermissionDto;
import top.waikin.mooc.admin.service.AdminCacheService;
import top.waikin.mooc.admin.service.PermissionService;
import top.waikin.mooc.entity.Permission;
import top.waikin.mooc.mapper.PermissionMapper;

/**
 * 后台权限 服务实现类
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

    @Autowired
    AdminCacheService adminCacheService;

    @Override
    public int create(PermissionDto permissionDto) {
        Permission permission = new Permission();
        BeanUtil.copyProperties(permissionDto, permission);
        save(permission);
        return permission.getId();
    }

    @Override
    public boolean delete(Integer id) {
        boolean b = removeById(id);
        // 删除缓存
        adminCacheService.deletePermissionListByPermission(id);
        return b;
    }

    @Override
    public boolean update(Integer id, PermissionDto permissionDto) {
        Permission permission = new Permission();
        BeanUtil.copyProperties(permissionDto, permission);
        permission.setId(id);
        boolean b = updateById(permission);
        // 删除缓存
        adminCacheService.deletePermissionListByPermission(id);
        return b;
    }

    @Override
    public IPage<Permission> page(Integer current, Integer size, String name) {
        Wrapper<Permission> wrapper = new LambdaQueryWrapper<Permission>().like(StrUtil.isNotBlank(name), Permission::getName, name);
        return page(new Page<>(current, size), wrapper);
    }


}
