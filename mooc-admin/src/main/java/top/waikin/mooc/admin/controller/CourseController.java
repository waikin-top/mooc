package top.waikin.mooc.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import top.waikin.mooc.admin.domain.CourseQuery;
import top.waikin.mooc.admin.service.CourseService;
import top.waikin.mooc.common.api.CommonPage;
import top.waikin.mooc.common.api.CommonResult;
import top.waikin.mooc.entity.Course;

/**
 * 课程管理 控制器
 */
@RestController
@Api(tags = "CourseController", description = "课程管理")
@RequestMapping("/course")
public class CourseController {
    @Autowired
    CourseService courseService;

    @ApiOperation("删除课程")
    @DeleteMapping("/{id}")
    public CommonResult delete(@PathVariable Integer id) {
        boolean b = courseService.removeById(id);
        return CommonResult.decide(b);
    }

    @ApiOperation("获取指定课程")
    @GetMapping("/{id}")
    public CommonResult<Course> getById(@PathVariable Integer id) {
        Course course = courseService.getById(id);
        return CommonResult.success(course);
    }

    @ApiOperation("根据课程名称以及课程查询参数分页查询课程列表")
    @GetMapping
    public CommonResult<CommonPage<Course>> page(@RequestParam(defaultValue = "1") Integer current,
                                                 @RequestParam(defaultValue = "10") Integer size,
                                                 @RequestParam(required = false) String name,
                                                 CourseQuery courseQuery) {
        IPage<Course> page = courseService.page(current, size, name, courseQuery);
        return CommonResult.success(CommonPage.restPage(page));
    }
}
