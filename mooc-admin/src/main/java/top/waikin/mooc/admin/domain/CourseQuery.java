package top.waikin.mooc.admin.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 课程分页查询条件
 */
@Data
public class CourseQuery {
    @ApiModelProperty("频道id")
    private Integer channelId;

    @ApiModelProperty("公开状态 0->不公开,1->公开")
    private Integer isPublic;

    @ApiModelProperty("创建时间开始")
    private LocalDateTime startTime;

    @ApiModelProperty("创建时间结束")
    private LocalDateTime endTime;

    @ApiModelProperty("加入课程人数降序")
    private Boolean enrollCountDesc;

    @ApiModelProperty("评分平均分降序")
    private Boolean averageMarkDesc;
}
