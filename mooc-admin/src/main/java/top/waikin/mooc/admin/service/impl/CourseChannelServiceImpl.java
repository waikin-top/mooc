package top.waikin.mooc.admin.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.waikin.mooc.admin.domain.CourseChannelDto;
import top.waikin.mooc.admin.service.CourseChannelService;
import top.waikin.mooc.entity.CourseChannel;
import top.waikin.mooc.mapper.CourseChannelMapper;

/**
 * 频道 服务实现类
 */
@Service
public class CourseChannelServiceImpl extends ServiceImpl<CourseChannelMapper, CourseChannel> implements CourseChannelService {

    @Override
    public int create(CourseChannelDto courseChannelDto) {
        CourseChannel courseChannel = new CourseChannel();
        BeanUtil.copyProperties(courseChannelDto, courseChannel);
        courseChannel.setCourseCount(0);
        save(courseChannel);
        return courseChannel.getId();
    }

    @Override
    public boolean update(Integer id, CourseChannelDto courseChannelDto) {
        CourseChannel courseChannel = new CourseChannel();
        BeanUtil.copyProperties(courseChannelDto, courseChannel);
        courseChannel.setId(id);
        return updateById(courseChannel);
    }
}
