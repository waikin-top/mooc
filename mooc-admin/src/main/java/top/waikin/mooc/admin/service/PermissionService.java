package top.waikin.mooc.admin.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import top.waikin.mooc.admin.domain.PermissionDto;
import top.waikin.mooc.entity.Permission;

/**
 * 后台权限 服务类
 */
public interface PermissionService extends IService<Permission> {

    int create(PermissionDto permissionDto);

    boolean delete(Integer id);

    boolean update(Integer id, PermissionDto permissionDto);

    IPage<Permission> page(Integer current, Integer size, String name);
}
