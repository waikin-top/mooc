package top.waikin.mooc.admin.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import top.waikin.mooc.admin.domain.CourseQuery;
import top.waikin.mooc.entity.Course;

/**
 * 课程 服务类
 */
public interface CourseService extends IService<Course> {
    IPage<Course> page(Integer current, Integer size, String name, CourseQuery courseQuery);
}
