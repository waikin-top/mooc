package top.waikin.mooc.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.waikin.mooc.admin.domain.CarouselDto;
import top.waikin.mooc.admin.service.HomeCacheService;
import top.waikin.mooc.common.api.CommonResult;
import top.waikin.mooc.entity.Carousel;
import top.waikin.mooc.entity.Course;

import java.util.List;

/**
 * 前台主页 控制器
 */
@Api(tags = "HomeController", description = "前台主页 控制器")
@RestController
@RequestMapping("/home")
public class HomeController {
    @Autowired
    HomeCacheService homeCacheService;

    @ApiOperation("添加推荐课程")
    @PostMapping("/recommend/{courseId}")
    public CommonResult createRecommendCourse(@PathVariable Integer courseId) {
        homeCacheService.createRecommendCourse(courseId);
        return CommonResult.success(null);
    }

    @ApiOperation("删除推荐课程")
    @DeleteMapping("/recommend/{courseId}")
    public CommonResult deleteRecommendCourse(@PathVariable Integer courseId) {
        homeCacheService.deleteRecommendCourse(courseId);
        return CommonResult.success(null);
    }

    @ApiOperation("获取课程推荐列表")
    @GetMapping("/recommend")
    public CommonResult<List<Course>> getRecommendCourseList() {
        List<Course> recommendCourseList = homeCacheService.getRecommendCourseList();
        return CommonResult.success(recommendCourseList);
    }

    @ApiOperation(value = "创建轮播图",notes = "返回的是列表长度")
    @PostMapping("/carousel")
    public CommonResult<Integer> createCarousel(@RequestBody @Validated CarouselDto carouselDto) {
        int index = homeCacheService.createCarousel(carouselDto);
        return CommonResult.success(index);
    }

    @ApiOperation(value = "删除轮播图",notes = "carouselIndex从0开始，0代表列表第一个元素")
    @DeleteMapping("/carousel/{carouselIndex}")
    public CommonResult<Integer> deleteCarousel(@PathVariable Integer carouselIndex) {
        homeCacheService.deleteCarousel(carouselIndex);
        return CommonResult.success(null);
    }

    @ApiOperation("修改轮播图")
    @PutMapping("/carousel/{carouselIndex}")
    public CommonResult updateCarousel(@PathVariable Integer carouselIndex,
                                       @RequestBody @Validated CarouselDto carouselDto) {
        homeCacheService.updateCarousel(carouselIndex, carouselDto);
        return CommonResult.success(null);
    }

    @ApiOperation("获取轮播图信息列表")
    @GetMapping("/carousel")
    public CommonResult<List<Carousel>> getCarouselList() {
        List<Carousel> carouselList = homeCacheService.getCarouselList();
        return CommonResult.success(carouselList);
    }
}
