package top.waikin.mooc.admin.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.waikin.mooc.admin.domain.CourseQuery;
import top.waikin.mooc.admin.service.CourseService;
import top.waikin.mooc.entity.Course;
import top.waikin.mooc.mapper.CourseMapper;


/**
 * 课程 服务实现类
 */
@Service
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements CourseService {
    @Override
    public IPage<Course> page(Integer current, Integer size, String name, CourseQuery courseQuery) {
        Wrapper<Course> wrapper = new LambdaQueryWrapper<Course>().like(StrUtil.isNotBlank(name), Course::getName, name)
                .eq(ObjectUtil.isNotNull(courseQuery.getChannelId()), Course::getChannelId, courseQuery.getChannelId())
                .eq(ObjectUtil.isNotNull(courseQuery.getIsPublic()), Course::getIsPublic, courseQuery.getIsPublic())
                .between(ObjectUtil.isNotNull(courseQuery.getStartTime()) && ObjectUtil.isNotNull(courseQuery.getEndTime()),
                        Course::getCreateTime, courseQuery.getStartTime(), courseQuery.getEndTime())
                .orderByDesc(ObjectUtil.isNotNull(courseQuery.getEnrollCountDesc()), Course::getEnrollCount)
                .orderByDesc(ObjectUtil.isNotNull(courseQuery.getAverageMarkDesc()), Course::getAverageMark);
        return page(new Page<>(current, size), wrapper);
    }
}
