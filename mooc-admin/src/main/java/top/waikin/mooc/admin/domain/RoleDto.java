package top.waikin.mooc.admin.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

/**
 * 角色参数
 */
@Data
public class RoleDto {

    @NotBlank(message = "角色名称不能为空")
    @ApiModelProperty("角色名称")
    private String name;

    @ApiModelProperty("描述")
    private String description;

    @Min(value = 0, message = "启用状态只能是0或1")
    @Max(value = 1, message = "启用状态只能是0或1")
    @ApiModelProperty("启用状态:0->禁用,1->启用")
    private Integer status;

    @ApiModelProperty("排序")
    private Integer sort;
}
