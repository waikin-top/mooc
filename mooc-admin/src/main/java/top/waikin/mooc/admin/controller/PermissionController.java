package top.waikin.mooc.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.waikin.mooc.admin.domain.PermissionDto;
import top.waikin.mooc.admin.service.PermissionService;
import top.waikin.mooc.common.api.CommonPage;
import top.waikin.mooc.common.api.CommonResult;
import top.waikin.mooc.entity.Permission;

/**
 * 后台权限 前端控制器
 */
@RestController
@RequestMapping("/permission")
@Api(tags = "PermissionController", description = "权限管理")
public class PermissionController {
    @Autowired
    PermissionService permissionService;

    @ApiOperation("创建权限")
    @PostMapping
    public CommonResult<Integer> create(@RequestBody @Validated PermissionDto permissionDto) {
        int id = permissionService.create(permissionDto);
        return CommonResult.success(id);
    }

    @ApiOperation("删除权限")
    @DeleteMapping("/{id}")
    public CommonResult delete(@PathVariable Integer id) {
        boolean b = permissionService.delete(id);
        return CommonResult.decide(b);
    }

    @ApiOperation("修改权限")
    @PutMapping("/{id}")
    public CommonResult update(@PathVariable Integer id, @RequestBody @Validated PermissionDto permissionDto) {
        boolean b = permissionService.update(id, permissionDto);
        return CommonResult.decide(b);
    }

    @ApiOperation("获取权限")
    @GetMapping("/{id}")
    public CommonResult<Permission> getById(@PathVariable Integer id) {
        Permission permission = permissionService.getById(id);
        return CommonResult.success(permission);
    }

    @ApiOperation("分页查询权限")
    @GetMapping
    public CommonResult<CommonPage<Permission>> page(@RequestParam(defaultValue = "1") Integer current,
                                                     @RequestParam(defaultValue = "10") Integer size,
                                                     @RequestParam(required = false) String name) {
        IPage<Permission> page = permissionService.page(current, size, name);
        return CommonResult.success(CommonPage.restPage(page));
    }

}
