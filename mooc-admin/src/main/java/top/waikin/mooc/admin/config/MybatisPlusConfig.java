package top.waikin.mooc.admin.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import top.waikin.mooc.config.BaseMybatisPlusConfig;

@Configuration
@EnableTransactionManagement
@MapperScan({"top.waikin.mooc.mapper"})
public class MybatisPlusConfig extends BaseMybatisPlusConfig {
}
