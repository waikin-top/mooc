package top.waikin.mooc.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.waikin.mooc.admin.domain.CourseChannelDto;
import top.waikin.mooc.admin.service.CourseChannelService;
import top.waikin.mooc.common.api.CommonResult;
import top.waikin.mooc.entity.CourseChannel;

import java.util.List;

/**
 * 课程频道控制器
 */
@Api(tags = "CourseChannelController", description = "课程频道控制器")
@RestController
@RequestMapping("/channel")
public class CourseChannelController {
    @Autowired
    CourseChannelService courseChannelService;

    @ApiOperation("创建频道")
    @PostMapping
    public CommonResult<Integer> create(@Validated @RequestBody CourseChannelDto courseChannelDto) {
        int id = courseChannelService.create(courseChannelDto);
        return CommonResult.success(id);
    }

    @ApiOperation("删除频道")
    @DeleteMapping("/{id}")
    public CommonResult delete(@PathVariable Integer id) {
        boolean b = courseChannelService.removeById(id);
        return CommonResult.decide(b);
    }

    @ApiOperation("修改频道")
    @PutMapping("/{id}")
    public CommonResult update(@PathVariable Integer id,
                               @Validated @RequestBody CourseChannelDto courseChannelDto) {
        boolean b = courseChannelService.update(id, courseChannelDto);
        return CommonResult.decide(b);
    }

    @ApiOperation("获取频道列表")
    @GetMapping
    public CommonResult<List<CourseChannel>> list() {
        List<CourseChannel> list = courseChannelService.list();
        return CommonResult.success(list);
    }
}
