package top.waikin.mooc.admin.service;

import top.waikin.mooc.entity.Admin;
import top.waikin.mooc.entity.Permission;

import java.util.List;

/**
 * 管理员缓存 服务类
 */
public interface AdminCacheService {
    /**
     * 保存管理员
     */
    void saveAdmin(Admin admin);

    /**
     * 删除管理员
     */
    void deleteAdmin(String telephone);

    /**
     * 获取管理员
     */
    Admin getAdmin(String telephone);

    /**
     * 保存验证码
     */
    void saveAuthCode(String telephone, String authCode);

    /**
     * 获取验证码
     */
    String getAuthCode(String telephone);

    /**
     * 保存权限列表
     */
    void savePermissionList(Integer adminId, List<Permission> permissionList);

    /**
     * 删除权限列表
     */
    void deletePermissionList(Integer adminId);

    /**
     * 获取权限列表
     */
    List<Permission> getPermissionList(Integer adminId);

    /**
     * 当角色信息发生改变时删除相关权限缓存
     */
    void deletePermissionListByRole(Integer roleId);

    /**
     * 当权限信息发生改变时删除相关权限缓存
     */
    void deletePermissionListByPermission(Integer permissionId);

}
