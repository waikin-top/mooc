package top.waikin.mooc.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.waikin.mooc.admin.service.AdminRoleRelationService;
import top.waikin.mooc.entity.AdminRoleRelation;
import top.waikin.mooc.entity.Permission;
import top.waikin.mooc.entity.Role;
import top.waikin.mooc.mapper.AdminRoleRelationMapper;

import java.util.List;

/**
 * 管理员和角色关系 服务实现类
 */
@Service
public class AdminRoleRelationServiceImpl extends ServiceImpl<AdminRoleRelationMapper, AdminRoleRelation> implements AdminRoleRelationService {

    @Override
    public List<Role> getRoleListByAdminId(Integer id) {
        return baseMapper.getRoleListByAdminId(id);
    }

    @Override
    public List<Permission> getPermissionListByAdminId(Integer adminId) {
        return baseMapper.getPermissionListByAdminId(adminId);
    }
}
