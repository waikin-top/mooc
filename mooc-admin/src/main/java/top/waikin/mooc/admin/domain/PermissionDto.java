package top.waikin.mooc.admin.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * 权限参数
 */
@Data
public class PermissionDto {

    @NotBlank(message = "资源名称不能为空")
    @ApiModelProperty("资源名称")
    private String name;

    @Pattern(regexp = "^/.*", message = "路径要以'/'开头")
    @ApiModelProperty("路径 /admin/**")
    private String url;

    @Pattern(regexp = "GET|POST|PUT|DELETE|ALL", message = "请求方法为 GET,POST,PUT,DELETE,ALL")
    @ApiModelProperty("请求方法 GET,POST,PUT,DELETE")
    private String method;

    @ApiModelProperty("描述")
    private String description;
}
