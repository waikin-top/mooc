package top.waikin.mooc.admin.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import top.waikin.mooc.admin.domain.CarouselDto;
import top.waikin.mooc.admin.service.CourseService;
import top.waikin.mooc.admin.service.HomeCacheService;
import top.waikin.mooc.common.service.RedisService;
import top.waikin.mooc.entity.Carousel;
import top.waikin.mooc.entity.Course;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class HomeCacheServiceImpl implements HomeCacheService {
    @Autowired
    RedisService redisService;

    @Autowired
    CourseService courseService;

    @Value("${redis.database}")
    private String REDIS_DATABASE;

    @Value("${redis.key.recommendCourseHash}")
    private String REDIS_KEY_RECOMMEND_COURSE_HASH;

    @Value("${redis.key.carouselList}")
    private String REDIS_KEY_CAROUSEL_LIST;

    @Override
    public void createRecommendCourse(Integer courseId) {
        String key = REDIS_DATABASE + ":" + REDIS_KEY_RECOMMEND_COURSE_HASH;
        Course course = courseService.getById(courseId);
        if (ObjectUtil.isNull(course)) {
            return;
        }
        redisService.hSet(key, courseId.toString(), course);
    }

    @Override
    public void deleteRecommendCourse(Integer courseId) {
        String key = REDIS_DATABASE + ":" + REDIS_KEY_RECOMMEND_COURSE_HASH;
        redisService.hDel(key, courseId.toString());
    }

    @Override
    public List<Course> getRecommendCourseList() {
        String key = REDIS_DATABASE + ":" + REDIS_KEY_RECOMMEND_COURSE_HASH;
        Map<Object, Object> objectObjectMap = redisService.hGetAll(key);
        return objectObjectMap.values().stream().map(o -> (Course) o).collect(Collectors.toList());
    }

    @Override
    public int createCarousel(CarouselDto carouselDto) {
        String key = REDIS_DATABASE + ":" + REDIS_KEY_CAROUSEL_LIST;
        Carousel carousel = new Carousel();
        BeanUtil.copyProperties(carouselDto, carousel);
        return redisService.lPush(key, carousel).intValue();
    }

    @Override
    public void deleteCarousel(Integer carouselIndex) {
        String key = REDIS_DATABASE + ":" + REDIS_KEY_CAROUSEL_LIST;
        Carousel carouse = (Carousel) redisService.lIndex(key, carouselIndex);
        if (ObjectUtil.isNull(carouse)) {
            return;
        }
        redisService.lRemove(key, 1L, carouse);
    }

    @Override
    public void updateCarousel(Integer carouselIndex, CarouselDto carouselDto) {
        String key = REDIS_DATABASE + ":" + REDIS_KEY_CAROUSEL_LIST;
        Carousel carousel = new Carousel();
        BeanUtil.copyProperties(carouselDto, carousel);
        redisService.lSet(key, carouselIndex, carousel);
    }

    @Override
    public List<Carousel> getCarouselList() {
        String key = REDIS_DATABASE + ":" + REDIS_KEY_CAROUSEL_LIST;
        List<Object> objectList = redisService.lRange(key, 0, -1);
        return objectList.stream().map(o -> (Carousel) o).collect(Collectors.toList());
    }
}
