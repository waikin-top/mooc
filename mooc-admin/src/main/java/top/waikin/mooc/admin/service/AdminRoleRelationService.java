package top.waikin.mooc.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.waikin.mooc.entity.AdminRoleRelation;
import top.waikin.mooc.entity.Permission;
import top.waikin.mooc.entity.Role;

import java.util.List;

/**
 * 管理员和角色关系 服务类
 */
public interface AdminRoleRelationService extends IService<AdminRoleRelation> {

    List<Role> getRoleListByAdminId(Integer id);

    List<Permission> getPermissionListByAdminId(Integer adminId);
}
