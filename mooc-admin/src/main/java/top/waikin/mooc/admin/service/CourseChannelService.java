package top.waikin.mooc.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.waikin.mooc.admin.domain.CourseChannelDto;
import top.waikin.mooc.entity.CourseChannel;

/**
 * 频道 服务类
 */
public interface CourseChannelService extends IService<CourseChannel> {

    int create(CourseChannelDto courseChannelDto);

    boolean update(Integer id, CourseChannelDto courseChannelDto);
}
