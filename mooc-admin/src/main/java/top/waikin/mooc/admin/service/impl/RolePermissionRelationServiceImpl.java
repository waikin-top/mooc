package top.waikin.mooc.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.waikin.mooc.admin.service.RolePermissionRelationService;
import top.waikin.mooc.entity.RolePermissionRelation;
import top.waikin.mooc.mapper.RolePermissionRelationMapper;

import java.util.List;

/**
 * 后台 角色和资源关系 服务实现类
 */
@Service
public class RolePermissionRelationServiceImpl extends ServiceImpl<RolePermissionRelationMapper, RolePermissionRelation> implements RolePermissionRelationService {

    @Override
    public List<Integer> getAdminIdListByPermissionId(Integer permissionId) {
        return baseMapper.getAdminIdListByPermissionId(permissionId);
    }
}
