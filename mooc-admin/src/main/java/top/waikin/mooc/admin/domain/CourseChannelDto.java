package top.waikin.mooc.admin.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;


@Data
public class CourseChannelDto {
    @NotBlank
    @ApiModelProperty("频道名称")
    private String name;

    @ApiModelProperty("排序")
    private Integer sort;
}
