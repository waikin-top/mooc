package top.waikin.mooc.admin.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;

/**
 * 用户信息参数
 */
@Data
public class AdminInfoDto {

    @NotBlank(message = "昵称不能为空")
    @Length(min = 3, max = 14, message = "昵称字符数为3-14位")
    @ApiModelProperty("昵称")
    private String nickname;

    @URL(message = "头像链接无效")
    @ApiModelProperty("头像")
    private String profile;
}
