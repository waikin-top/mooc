package top.waikin.mooc.admin.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

@Data
public class CarouselDto {

    @ApiModelProperty("轮播图标题")
    private String title;

    @URL
    @ApiModelProperty("轮播图图片链接")
    private String pic;

    @URL
    @ApiModelProperty("轮播图跳转链接")
    private String url;
}
