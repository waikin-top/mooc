package top.waikin.mooc.portal;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import top.waikin.mooc.common.service.COSService;
import top.waikin.mooc.common.service.SMSService;

import javax.sql.DataSource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MoocPortalApplicationTest {
    @Autowired
    SMSService smsService;

    @Autowired
    COSService cosService;

    @Test
    public void test() {
        System.out.println(cosService.generateUploadUrl());
    }

    @Autowired(required = false)
    DataSource dataSource;

    @Test
    public void testDataSource() {
        System.out.println(dataSource.getClass());
    }
}
