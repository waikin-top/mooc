package top.waikin.mooc.portal.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.waikin.mooc.common.api.CommonResult;
import top.waikin.mooc.entity.Carousel;
import top.waikin.mooc.entity.Course;
import top.waikin.mooc.entity.CourseRankTuple;
import top.waikin.mooc.portal.service.HomeCacheService;
import top.waikin.mooc.portal.service.RankCacheService;

import java.util.List;


@Api(tags = "HomeController", description = "主页 控制器")
@RestController
@RequestMapping("/home")
public class HomeController {
    @Autowired
    HomeCacheService homeCacheService;

    @Autowired
    RankCacheService rankCacheService;

    @ApiOperation("获取课程推荐列表")
    @GetMapping("/recommend")
    public CommonResult<List<Course>> getRecommendCourseList() {
        List<Course> recommendCourseList = homeCacheService.getRecommendCourseList();
        return CommonResult.success(recommendCourseList);
    }


    @ApiOperation("获取轮播图列表")
    @GetMapping("/carousel")
    public CommonResult<List<Carousel>> getCarouselList() {
        List<Carousel> carouselList = homeCacheService.getCarouselList();
        return CommonResult.success(carouselList);
    }

    @ApiOperation("获取近7天课程排行")
    @GetMapping("/weekRank")
    public CommonResult<List<CourseRankTuple>> getWeekRankCourseList() {
        List<CourseRankTuple> weakCourseRankList = rankCacheService.getWeekRankCourseList();
        return CommonResult.success(weakCourseRankList);
    }
}
