package top.waikin.mooc.portal.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用户信息
 */
@Data
public class UserInfoDto {
    @ApiModelProperty("昵称")
    private String nickname;

    @ApiModelProperty("性别 0-位置 1-男性 2-女性")
    private Integer gender;

    @ApiModelProperty("头像")
    private String profile;
}
