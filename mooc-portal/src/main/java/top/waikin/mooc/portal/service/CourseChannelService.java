package top.waikin.mooc.portal.service;

import top.waikin.mooc.entity.CourseChannel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 频道 服务类
 */
public interface CourseChannelService extends IService<CourseChannel> {

    void increaseCourseCount(Integer id);

    void decreaseCourseCount(Integer id);
}
