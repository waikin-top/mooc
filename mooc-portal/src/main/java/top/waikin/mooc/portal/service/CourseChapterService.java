package top.waikin.mooc.portal.service;

import top.waikin.mooc.entity.CourseChapter;
import com.baomidou.mybatisplus.extension.service.IService;
import top.waikin.mooc.portal.domain.CourseChapterDto;

import java.util.List;

/**
 * 课程章节 服务类
 */
public interface CourseChapterService extends IService<CourseChapter> {

    int create(Integer courseId, CourseChapterDto courseChapterDto);

    boolean delete(Integer courseId, Integer id);

    boolean update(Integer courseId, Integer id, CourseChapterDto courseChapterDto);

    List<CourseChapter> sortList(Integer courseId);
}
