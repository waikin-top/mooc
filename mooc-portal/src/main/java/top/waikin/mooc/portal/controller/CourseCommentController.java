package top.waikin.mooc.portal.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.waikin.mooc.common.api.CommonResult;
import top.waikin.mooc.entity.CourseComment;
import top.waikin.mooc.portal.domain.CourseCommentDto;
import top.waikin.mooc.portal.service.CourseCommentService;

import java.util.List;

@Api(tags = "CourseCommentController",description = "课程评论 控制器")
@RestController
@RequestMapping("/course/{courseId}/comment")
public class CourseCommentController {
    @Autowired
    CourseCommentService courseCommentService;

    @ApiOperation("创建课程评论")
    @PostMapping
    public CommonResult<Integer> create(@PathVariable Integer courseId,
                                        @RequestBody @Validated CourseCommentDto courseCommentDto) {
        int id = courseCommentService.create(courseId, courseCommentDto);
        return CommonResult.success(id);
    }

    @ApiOperation("删除课程评论")
    @DeleteMapping("/{id}")
    public CommonResult delete(@PathVariable Integer courseId,
                               @PathVariable Integer id) {
        boolean b = courseCommentService.delete(courseId, id);
        return CommonResult.decide(b);
    }

    @ApiOperation("更新课程评论")
    @PutMapping("/{id}")
    public CommonResult update(@PathVariable Integer courseId,
                               @PathVariable Integer id,
                               @RequestBody @Validated CourseCommentDto courseCommentDto) {
        boolean b = courseCommentService.update(courseId, id, courseCommentDto);
        return CommonResult.decide(b);
    }

    @ApiOperation(value = "获取课程评论列表", notes = "登录用户如果有评论，会在列表的第一条")
    @GetMapping
    public CommonResult<List<CourseComment>> list(@PathVariable Integer courseId) {
        List<CourseComment> list = courseCommentService.list(courseId);
        return CommonResult.success(list);
    }
}
