package top.waikin.mooc.portal.controller;

import cn.dev33.satoken.stp.SaTokenInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.waikin.mooc.common.api.CommonResult;
import top.waikin.mooc.entity.User;
import top.waikin.mooc.portal.domain.UserInfoDto;
import top.waikin.mooc.portal.service.UserService;
import top.waikin.mooc.security.util.PrivilegeUtil;

/**
 * 单点登录 控制器
 */
@Validated
@RestController
@Api(tags = "SSOController", description = "单点登录")
@RequestMapping("/sso")
public class SSOController {
    @Autowired
    UserService userService;

    @ApiOperation("获取验证码")
    @PostMapping("/getAuthCode")
    public CommonResult getAuthCode(@RequestParam String telephone) {
        userService.generateAuthCode(telephone);
        return CommonResult.success(null);
    }

    @ApiOperation("注册")
    @PostMapping("/register")
    public CommonResult<SaTokenInfo> register(@RequestParam @Length(min = 11, max = 11, message = "手机号码为11位") String telephone,
                                              @RequestParam @Length(min = 8, max = 16, message = "密码长度为8-16位") String password,
                                              @RequestParam String authCode) {
        userService.register(telephone, password, authCode);
        return CommonResult.success(PrivilegeUtil.getTokenInfo(), "登录成功");
    }

    @ApiOperation("登录")
    @PostMapping("/login")
    public CommonResult<SaTokenInfo> login(@RequestParam String telephone,
                                           @RequestParam String password) {
        userService.login(telephone, password);
        return CommonResult.success(PrivilegeUtil.getTokenInfo(), "登录成功");
    }

    @ApiOperation("验证码登录")
    @PostMapping("/loginByAuthCode")
    public CommonResult<SaTokenInfo> loginByAuthCode(@RequestParam String telephone,
                                                     @RequestParam String authCode) {
        userService.loginByAuthCode(telephone, authCode);
        return CommonResult.success(PrivilegeUtil.getTokenInfo(), "登录成功");
    }

    @ApiOperation("修改密码")
    @PostMapping("/updatePassword")
    public CommonResult updatePassword(@RequestParam String telephone,
                                       @RequestParam @Length(min = 8, max = 16, message = "密码长度为8-16位") String password,
                                       @RequestParam String authCode) {
        userService.updatePassword(telephone, password, authCode);
        return CommonResult.success(null, "修改密码成功");
    }

    @ApiOperation("获取用户信息")
    @GetMapping("/info")
    public CommonResult<User> info() {
        User user = userService.getCurrentUser();
        user.setPassword(null);
        return CommonResult.success(user);
    }

    @ApiOperation("更新用户信息")
    @PutMapping("/info")
    public CommonResult updateInfo(@RequestBody UserInfoDto userInfoDto) {
        userService.updateUserInfo(userInfoDto);
        return CommonResult.success(null);
    }

    @ApiOperation("更新token")
    @GetMapping("/refreshToken")
    public CommonResult<SaTokenInfo> refreshToken() {
        User user = userService.getCurrentUser();
        PrivilegeUtil.login(user.getTelephone());
        return CommonResult.success(PrivilegeUtil.getTokenInfo());
    }
}
