package top.waikin.mooc.portal.config;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import top.waikin.mooc.common.config.BaseSwaggerConfig;
import top.waikin.mooc.common.domain.SwaggerProperties;


/**
 * Swagger配置
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig extends BaseSwaggerConfig {
    @Override
    public SwaggerProperties swaggerProperties() {
        return SwaggerProperties.builder()
                .apiBasePackage("top.waikin.mooc.portal.controller")
                .title("mooc前台系统")
                .description("mooc前台相关接口文档")
                .contactName("WaiKin")
                .version("1.0")
                .enableSecurity(true)
                .build();
    }

    @Bean
    public BeanPostProcessor springfoxHandlerProviderBeanPostProcessor() {
        return generateBeanPostProcessor();
    }

}
