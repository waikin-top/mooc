package top.waikin.mooc.portal.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.waikin.mooc.common.api.CommonPage;
import top.waikin.mooc.common.api.CommonResult;
import top.waikin.mooc.entity.Course;
import top.waikin.mooc.portal.domain.UserCourseRelationDto;
import top.waikin.mooc.portal.service.CourseService;
import top.waikin.mooc.portal.service.UserCourseRelationService;

/**
 * 我的学习 控制器
 */
@RestController
@Api(tags = "LearnController", description = "我的学习控制器")
@RequestMapping("/learn")
public class LearnController {

    @Autowired
    UserCourseRelationService userCourseRelationService;

    @Autowired
    CourseService courseService;

    @ApiOperation("加入课程")
    @PostMapping
    public CommonResult join(@RequestBody @Validated UserCourseRelationDto userCourseRelationDto) {
        boolean b = userCourseRelationService.join(userCourseRelationDto);
        return CommonResult.decide(b);
    }

    @ApiOperation("退出课程")
    @DeleteMapping("/{courseId}")
    public CommonResult drop(@PathVariable Integer courseId) {
        boolean b = userCourseRelationService.drop(courseId);
        return CommonResult.decide(b);
    }

    @ApiOperation("获取指定课程")
    @GetMapping("/{courseId}")
    public CommonResult<Course> getById(@PathVariable Integer courseId) {
        Course course = courseService.getById(courseId);
        return CommonResult.success(course);
    }

    @ApiOperation("根据课程名称分页查询学习课程")
    @GetMapping
    public CommonResult<CommonPage<Course>> page(@RequestParam(defaultValue = "1") Integer current,
                                                 @RequestParam(defaultValue = "10") Integer size,
                                                 @RequestParam(required = false) String name) {
        IPage<Course> page = userCourseRelationService.page(current, size, name);
        return CommonResult.success(CommonPage.restPage(page));
    }
}
