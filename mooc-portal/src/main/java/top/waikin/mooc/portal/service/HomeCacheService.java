package top.waikin.mooc.portal.service;

import top.waikin.mooc.entity.Carousel;
import top.waikin.mooc.entity.Course;

import java.util.List;

/**
 * 主页服务类
 */
public interface HomeCacheService {

    List<Course> getRecommendCourseList();

    List<Carousel> getCarouselList();
}
