package top.waikin.mooc.portal.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;

@Data
public class CourseDataDto {
    @NotBlank(message = "文件名称不能为空")
    @ApiModelProperty("文件名称")
    private String name;

    @ApiModelProperty("扩展名")
    private String extension;

    @NotBlank
    @URL
    @ApiModelProperty("路径")
    private String url;

    @ApiModelProperty("排序")
    private Integer sort;
}
