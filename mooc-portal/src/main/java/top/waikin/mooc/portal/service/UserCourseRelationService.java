package top.waikin.mooc.portal.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.transaction.annotation.Transactional;
import top.waikin.mooc.entity.Course;
import top.waikin.mooc.entity.UserCourseRelation;
import com.baomidou.mybatisplus.extension.service.IService;
import top.waikin.mooc.portal.domain.UserCourseRelationDto;

/**
 * 用户和课程关系（选课） 服务类
 */
public interface UserCourseRelationService extends IService<UserCourseRelation> {

    @Transactional
    boolean join(UserCourseRelationDto userCourseRelationDto);

    @Transactional
    boolean drop(Integer courseId);

    IPage<Course> page(Integer current, Integer size, String name);
}
