package top.waikin.mooc.portal.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.transaction.annotation.Transactional;
import top.waikin.mooc.entity.Course;
import com.baomidou.mybatisplus.extension.service.IService;
import top.waikin.mooc.portal.domain.CourseDto;
import top.waikin.mooc.portal.domain.CourseQuery;

/**
 * 课程 服务类
 */
public interface CourseService extends IService<Course> {

    @Transactional
    int create(CourseDto courseDto);

    @Transactional
    boolean delete(Integer id);

    @Transactional
    boolean update(Integer id, CourseDto courseDto);

    IPage<Course> pageTeach(Integer current, Integer size, String name);

    IPage<Course> pagePublic(Integer current, Integer size, String name, CourseQuery courseQuery);

    void checkPermission(Integer id);


    void increaseEnrollCount(Integer courseId);

    void decreaseEnrollCount(Integer courseId);

    void addMark(Integer courseId, Integer mark);

    void deleteMark(Integer courseId, Integer mark);

    void updateMark(Integer courseId, Integer oldMark, Integer newMark);
}
