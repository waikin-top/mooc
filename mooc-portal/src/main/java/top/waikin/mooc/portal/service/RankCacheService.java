package top.waikin.mooc.portal.service;

import top.waikin.mooc.entity.CourseRankTuple;

import java.util.List;

public interface RankCacheService {
    void increaseCourseRank(Integer courseId);

    List<CourseRankTuple> getWeekRankCourseList();
}
