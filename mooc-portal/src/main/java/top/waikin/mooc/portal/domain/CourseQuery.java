package top.waikin.mooc.portal.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CourseQuery {
    @ApiModelProperty("频道id")
    private Integer channelId;

    @ApiModelProperty("加入课程人数降序")
    private Boolean enrollCountDesc;

    @ApiModelProperty("评分平均分降序")
    private Boolean averageMarkDesc;
}
