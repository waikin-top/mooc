package top.waikin.mooc.portal.config;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import top.waikin.mooc.common.config.BaseRedisConfig;

@EnableCaching
@Configuration
public class RedisConfig extends BaseRedisConfig {
}
