package top.waikin.mooc.portal.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class CourseChapterDto {
    @NotNull(message = "下一章节id不能为空 没有下一章节为-1")
    @ApiModelProperty("下一章节id 没有下一章节为-1")
    private Integer nextId;

    @NotEmpty
    @ApiModelProperty("章节名称")
    private String name;

    @ApiModelProperty("章节内容")
    private String content;
}
