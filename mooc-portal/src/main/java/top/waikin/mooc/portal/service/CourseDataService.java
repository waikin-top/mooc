package top.waikin.mooc.portal.service;

import top.waikin.mooc.entity.CourseData;
import com.baomidou.mybatisplus.extension.service.IService;
import top.waikin.mooc.portal.domain.CourseDataDto;

import java.util.List;

/**
 * 课程资料 服务类
 */
public interface CourseDataService extends IService<CourseData> {

    int create(Integer courseId, CourseDataDto courseDataDto);

    boolean delete(Integer courseId, Integer id);

    boolean update(Integer courseId, Integer id, CourseDataDto courseDataDto);

    List<CourseData> list(Integer courseId);
}
