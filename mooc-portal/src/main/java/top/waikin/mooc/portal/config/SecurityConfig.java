package top.waikin.mooc.portal.config;


import cn.dev33.satoken.router.SaRouter;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import top.waikin.mooc.common.api.CommonResult;
import top.waikin.mooc.common.api.ResultCode;
import top.waikin.mooc.entity.Admin;
import top.waikin.mooc.entity.User;
import top.waikin.mooc.portal.service.UserService;
import top.waikin.mooc.security.config.BaseSecurityConfig;
import top.waikin.mooc.security.util.PrivilegeUtil;


@Configuration
public class SecurityConfig extends BaseSecurityConfig {
    @Autowired
    UserService userService;

    @Override
    public void run(Object r) {
        // 判断用户登录状态
        if (!PrivilegeUtil.isLogin()) {
            SaRouter.back(JSONUtil.parseObj(CommonResult.failed(ResultCode.UNAUTHORIZED), false));
        }
        User user = userService.getCurrentUser();
        // 判断用户是否被禁用
        if (ObjectUtil.notEqual(user.getStatus(), 1)) {
            SaRouter.back(JSONUtil.parseObj(CommonResult.failed("该用户被禁用"), false));
        }
    }
}
