package top.waikin.mooc.portal.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import top.waikin.mooc.entity.CourseChannel;
import top.waikin.mooc.mapper.CourseChannelMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.waikin.mooc.portal.service.CourseChannelService;

/**
 * 频道 服务实现类
 */
@Service
public class CourseChannelServiceImpl extends ServiceImpl<CourseChannelMapper, CourseChannel> implements CourseChannelService {

    @Override
    public void increaseCourseCount(Integer id) {
        Wrapper<CourseChannel> wrapper = new LambdaUpdateWrapper<CourseChannel>().eq(CourseChannel::getId, id)
                .setSql("course_count = course_count + 1");
        update(wrapper);
    }

    @Override
    public void decreaseCourseCount(Integer id) {
        Wrapper<CourseChannel> wrapper = new LambdaUpdateWrapper<CourseChannel>().eq(CourseChannel::getId, id)
                .setSql("course_count = course_count - 1");
        update(wrapper);
    }
}
