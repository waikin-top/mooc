package top.waikin.mooc.portal.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.waikin.mooc.common.api.CommonResult;
import top.waikin.mooc.entity.CourseData;
import top.waikin.mooc.portal.service.CourseDataService;

import java.util.List;

/**
 * 单点登录 控制器
 */
@RestController
@Api(tags = "LearnDataController", description = "学习课程资料控制器")
@RequestMapping("/learn/{courseId}/data")
public class LearnDataController {
    @Autowired
    CourseDataService courseDataService;

    @ApiOperation("获取课程资料列表")
    @GetMapping
    public CommonResult<List<CourseData>> list(@PathVariable Integer courseId) {
        List<CourseData> dataList = courseDataService.list(courseId);
        return CommonResult.success(dataList);
    }

}
