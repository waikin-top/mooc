package top.waikin.mooc.portal.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.waikin.mooc.common.api.CommonResult;
import top.waikin.mooc.common.service.COSService;

import java.net.URL;

/**
 * 文件存储 控制器
 */
@RestController
@Api(tags = "StorageController", description = "文件存储 控制器")
@RequestMapping("/storage")
public class StorageController {
    @Autowired
    COSService cosService;

    @ApiOperation("获取上传链接")
    @GetMapping("/generateUploadUrl")
    public CommonResult<URL> generateUploadUrl() {
        URL url = cosService.generateUploadUrl();
        return CommonResult.success(url);
    }
}
