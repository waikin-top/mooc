package top.waikin.mooc.portal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 前台启动类
 */

@SpringBootApplication(scanBasePackages = "top.waikin.mooc")
public class MoocPortalApplication {
    public static void main(String[] args) {
        SpringApplication.run(MoocPortalApplication.class, args);
    }
}
