package top.waikin.mooc.portal.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.waikin.mooc.common.api.CommonResult;
import top.waikin.mooc.entity.CourseChapter;
import top.waikin.mooc.portal.domain.CourseChapterDto;
import top.waikin.mooc.portal.service.CourseChapterService;

import java.util.List;

/**
 * 教学课程章节 控制器
 */
@RestController
@Api(tags = "TeachChapterController", description = "教学课程章节控制器")
@RequestMapping("/teach/{courseId}/chapter")
public class TeachChapterController {
    @Autowired
    CourseChapterService courseChapterService;

    @ApiOperation("创建章节")
    @PostMapping
    public CommonResult<Integer> create(@PathVariable Integer courseId,
                                        @RequestBody @Validated CourseChapterDto courseChapterDto) {
        int id = courseChapterService.create(courseId, courseChapterDto);
        return CommonResult.success(id);
    }


    @ApiOperation("删除章节")
    @DeleteMapping("/{id}")
    public CommonResult delete(@PathVariable Integer courseId,
                               @PathVariable Integer id) {
        boolean b = courseChapterService.delete(courseId, id);
        return CommonResult.decide(b);
    }

    @ApiOperation("修改章节")
    @PutMapping("/{id}")
    public CommonResult update(@PathVariable Integer courseId,
                               @PathVariable Integer id,
                               @RequestBody @Validated CourseChapterDto courseChapterDto) {
        boolean b = courseChapterService.update(courseId, id, courseChapterDto);
        return CommonResult.decide(b);
    }

    @ApiOperation("获取指定章节")
    @GetMapping("/{id}")
    public CommonResult<CourseChapter> getById(@PathVariable Integer courseId,
                                               @PathVariable Integer id) {
        CourseChapter chapter = courseChapterService.getById(id);
        return CommonResult.success(chapter);
    }

    @ApiOperation("获取章节列表")
    @GetMapping
    public CommonResult<List<CourseChapter>> list(@PathVariable Integer courseId) {
        List<CourseChapter> page = courseChapterService.sortList(courseId);
        return CommonResult.success(page);
    }

}
