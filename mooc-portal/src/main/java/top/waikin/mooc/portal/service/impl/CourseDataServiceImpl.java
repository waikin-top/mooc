package top.waikin.mooc.portal.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import top.waikin.mooc.entity.CourseData;
import top.waikin.mooc.mapper.CourseDataMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.waikin.mooc.portal.domain.CourseDataDto;
import top.waikin.mooc.portal.service.CourseDataService;
import top.waikin.mooc.portal.service.CourseService;

import java.util.List;

/**
 * 课程资料 服务实现类
 */
@Service
public class CourseDataServiceImpl extends ServiceImpl<CourseDataMapper, CourseData> implements CourseDataService {
    @Autowired
    CourseService courseService;


    @Override
    public int create(Integer courseId, @Validated CourseDataDto courseDataDto) {
        courseService.checkPermission(courseId);
        CourseData courseData = new CourseData();
        BeanUtil.copyProperties(courseDataDto, courseData);
        courseData.setCourseId(courseId);
        save(courseData);
        return courseData.getCourseId();
    }

    @Override
    public boolean delete(Integer courseId, Integer id) {
        courseService.checkPermission(courseId);
        Wrapper<CourseData> wrapper = new LambdaQueryWrapper<CourseData>().eq(CourseData::getCourseId, courseId)
                .eq(CourseData::getId, id);
        return remove(wrapper);
    }

    @Override
    public boolean update(Integer courseId, Integer id, CourseDataDto courseDataDto) {
        courseService.checkPermission(courseId);
        CourseData courseData = new CourseData();
        BeanUtil.copyProperties(courseDataDto, courseData);
        Wrapper<CourseData> wrapper = new LambdaQueryWrapper<CourseData>().eq(CourseData::getCourseId, courseId)
                .eq(CourseData::getId, id);
        return update(courseData, wrapper);
    }

    @Override
    public List<CourseData> list(Integer courseId) {
        Wrapper<CourseData> wrapper = new LambdaQueryWrapper<CourseData>().eq(CourseData::getCourseId, courseId);
        return list(wrapper);
    }


}
