package top.waikin.mooc.portal.service;

import top.waikin.mooc.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import top.waikin.mooc.portal.domain.UserInfoDto;

/**
 * 用户 服务类
 */
public interface UserService extends IService<User> {
    /**
     * 登录
     */
    void login(String telephone,String password);

    /**
     * 验证码登录
     */
    void loginByAuthCode(String telephone,String authCode);

    /**
     * 注册
     */
    void register(String telephone,String password,String authCode);

    /**
     * 获取验证码
     */
    void generateAuthCode(String telephone);

    /**
     * 更新密码
     */
    void updatePassword(String telephone,String password,String authCode);

    /**
     * 更新用户信息
     */
    void updateUserInfo(UserInfoDto userInfoDto);

    /**
     * 获取当前用户
     */
    User getCurrentUser();

    /**
     * 手机号码获取用户
     */
    User getByTelephone(String telephone);
}
