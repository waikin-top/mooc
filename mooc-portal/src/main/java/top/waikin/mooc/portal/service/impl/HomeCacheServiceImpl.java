package top.waikin.mooc.portal.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import top.waikin.mooc.common.service.RedisService;
import top.waikin.mooc.entity.Carousel;
import top.waikin.mooc.entity.Course;
import top.waikin.mooc.portal.service.CourseService;
import top.waikin.mooc.portal.service.HomeCacheService;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class HomeCacheServiceImpl implements HomeCacheService {
    @Autowired
    RedisService redisService;

    @Autowired
    CourseService courseService;

    @Value("${redis.database}")
    private String REDIS_DATABASE;

    @Value("${redis.key.recommendCourseHash}")
    private String REDIS_KEY_RECOMMEND_COURSE_HASH;

    @Value("${redis.key.carouselList}")
    private String REDIS_KEY_CAROUSEL_LIST;


    @Override
    public List<Course> getRecommendCourseList() {
        String key = REDIS_DATABASE + ":" + REDIS_KEY_RECOMMEND_COURSE_HASH;
        Map<Object, Object> objectObjectMap = redisService.hGetAll(key);
        return objectObjectMap.values().stream().map(o -> (Course) o).collect(Collectors.toList());
    }

    @Override
    public List<Carousel> getCarouselList() {
        String key = REDIS_DATABASE + ":" + REDIS_KEY_CAROUSEL_LIST;
        List<Object> objectList = redisService.lRange(key, 0, -1);
        return objectList.stream().map(o -> (Carousel) o).collect(Collectors.toList());
    }
}
