package top.waikin.mooc.portal.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 课程参数
 */
@Data
public class CourseDto {

    @NotNull(message = "频道id不能为空")
    @ApiModelProperty("频道id")
    private Integer channelId;

    @NotBlank(message = "课程名称不能为空")
    @ApiModelProperty("课程名称")
    private String name;

    @ApiModelProperty("课程描述")
    private String description;

    @ApiModelProperty("课程概述")
    private String summary;

    @URL(message = "课程图片链接无效")
    @ApiModelProperty("课程图片")
    private String pic;

    @Min(value = 0, message = "公开状态只能为0或1")
    @Max(value = 1, message = "公开状态只能为0或1")
    @ApiModelProperty("公开状态 0->不公开,1->公开")
    private Integer isPublic;
}
