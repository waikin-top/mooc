package top.waikin.mooc.portal.config;

import top.waikin.mooc.config.BaseMybatisPlusConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@MapperScan({"top.waikin.mooc.mapper"})
public class MybatisPlusConfig extends BaseMybatisPlusConfig {
}
