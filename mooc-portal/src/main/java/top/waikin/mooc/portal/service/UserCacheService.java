package top.waikin.mooc.portal.service;

import top.waikin.mooc.entity.User;

/**
 * 用户缓存 服务类
 */
public interface UserCacheService {
    /**
     * 保存用户
     */
    void saveUser(User user);

    /**
     * 删除用户
     */
    void deleteUser(String telephone);

    /**
     * 获取用户
     */
    User getUser(String telephone);

    /**
     * 保存验证码
     */
    void saveAuthCode(String telephone, String authCode);

    /**
     * 获取验证码
     */
    String getAuthCode(String telephone);
}
