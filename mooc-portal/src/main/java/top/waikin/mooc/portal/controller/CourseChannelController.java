package top.waikin.mooc.portal.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.waikin.mooc.common.api.CommonResult;
import top.waikin.mooc.entity.CourseChannel;
import top.waikin.mooc.portal.service.CourseChannelService;

import java.util.List;

/**
 * 课程频道 控制器
 */
@Api(tags = "CourseChannelController", description = "课程频道 控制器")
@RestController
@RequestMapping("/channel")
public class CourseChannelController {
    @Autowired
    CourseChannelService courseChannelService;

    @ApiOperation("获取课程频道列表")
    @GetMapping
    public CommonResult<List<CourseChannel>> list() {
        List<CourseChannel> channelList = courseChannelService.list();
        return CommonResult.success(channelList);
    }
}
