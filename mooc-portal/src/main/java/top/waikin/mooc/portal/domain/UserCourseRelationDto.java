package top.waikin.mooc.portal.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 用户和课程关系参数
 */
@Data
public class UserCourseRelationDto {

    @NotNull(message = "课程id不能为空")
    @ApiModelProperty("课程id")
    private Integer courseId;

}
