package top.waikin.mooc.portal.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.waikin.mooc.common.api.CommonPage;
import top.waikin.mooc.common.api.CommonResult;
import top.waikin.mooc.entity.Course;
import top.waikin.mooc.portal.domain.CourseDto;
import top.waikin.mooc.portal.service.CourseService;

/**
 * 我的教学 控制器
 */
@RestController
@Api(tags = "TeachController", description = "我的教学")
@RequestMapping("/teach")
public class TeachController {

    @Autowired
    CourseService courseService;

    @ApiOperation("创建课程")
    @PostMapping
    public CommonResult<Integer> create(@RequestBody @Validated CourseDto courseDto) {
        int id = courseService.create(courseDto);
        return CommonResult.success(id);
    }

    @ApiOperation("删除课程")
    @DeleteMapping("/{id}")
    public CommonResult delete(@PathVariable Integer id) {
        boolean b = courseService.delete(id);
        return CommonResult.decide(b);
    }

    @ApiOperation("修改课程")
    @PutMapping("/{id}")
    public CommonResult update(@PathVariable Integer id, @RequestBody @Validated CourseDto courseDto) {
        boolean b = courseService.update(id, courseDto);
        return CommonResult.decide(b);
    }


    @ApiOperation("获取指定课程")
    @GetMapping("/{id}")
    public CommonResult<Course> getById(@PathVariable Integer id) {
        Course course = courseService.getById(id);
        return CommonResult.success(course);
    }

    @ApiOperation("根据课程名称分页查询教学课程")
    @GetMapping
    public CommonResult<CommonPage<Course>> page(@RequestParam(defaultValue = "1") Integer current,
                                                 @RequestParam(defaultValue = "10") Integer size,
                                                 @RequestParam(required = false) String name) {
        IPage<Course> page = courseService.pageTeach(current, size, name);
        return CommonResult.success(CommonPage.restPage(page));
    }
}
