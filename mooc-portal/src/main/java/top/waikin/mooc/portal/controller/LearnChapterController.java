package top.waikin.mooc.portal.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.waikin.mooc.common.api.CommonResult;
import top.waikin.mooc.entity.CourseChapter;
import top.waikin.mooc.portal.service.CourseChapterService;

import java.util.List;

/**
 * 学习课程章节 控制器
 */
@RestController
@Api(tags = "LearnChapterController", description = "学习课程章节控制器")
@RequestMapping("/learn/{courseId}/chapter")
public class LearnChapterController {
    @Autowired
    CourseChapterService courseChapterService;

    @ApiOperation("获取指定章节")
    @GetMapping("/{id}")
    public CommonResult<CourseChapter> getById(@PathVariable Integer courseId, @PathVariable Integer id) {
        CourseChapter chapter = courseChapterService.getById(id);
        return CommonResult.success(chapter);
    }


    @ApiOperation("获取章节列表")
    @GetMapping
    public CommonResult<List<CourseChapter>> list(@PathVariable Integer courseId) {
        List<CourseChapter> chapterList = courseChapterService.sortList(courseId);
        return CommonResult.success(chapterList);
    }
}
