package top.waikin.mooc.portal.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.waikin.mooc.common.exception.ApiException;
import top.waikin.mooc.common.exception.Asserts;
import top.waikin.mooc.entity.CourseComment;
import top.waikin.mooc.entity.User;
import top.waikin.mooc.mapper.CourseCommentMapper;
import top.waikin.mooc.portal.domain.CourseCommentDto;
import top.waikin.mooc.portal.service.CourseCommentService;
import top.waikin.mooc.portal.service.CourseService;
import top.waikin.mooc.portal.service.UserService;

import java.util.List;

/**
 * 课程评论 服务实现类
 */
@Service
public class CourseCommentServiceImpl extends ServiceImpl<CourseCommentMapper, CourseComment> implements CourseCommentService {
    @Autowired
    UserService userService;

    @Autowired
    CourseService courseService;

    @Override
    public int create(Integer courseId, CourseCommentDto courseCommentDto) {
        User user = userService.getCurrentUser();
        // 是否已经评论过
        Wrapper<CourseComment> wrapper = new LambdaQueryWrapper<CourseComment>().eq(CourseComment::getCourseId, courseId)
                .eq(CourseComment::getUserId, user.getId());
        CourseComment one = getOne(wrapper);
        if (ObjectUtil.isNotNull(one)) {
            Asserts.fail("你已经评论过了");
        }
        CourseComment courseComment = new CourseComment();
        BeanUtil.copyProperties(courseCommentDto, courseComment);
        courseComment.setCourseId(courseId);
        courseComment.setUserId(user.getId());
        save(courseComment);
        courseService.addMark(courseId, courseComment.getMark());
        return courseComment.getId();
    }

    @Override
    public boolean delete(Integer courseId, Integer id) {
        User user = userService.getCurrentUser();
        Wrapper<CourseComment> wrapper = new LambdaQueryWrapper<CourseComment>().eq(CourseComment::getId, id)
                .eq(CourseComment::getUserId, user.getId());
        CourseComment toDelete = getOne(wrapper);
        if (ObjectUtil.isNull(toDelete)) {
            return false;
        }
        removeById(toDelete);
        courseService.deleteMark(courseId, toDelete.getMark());
        return true;
    }

    @Override
    public boolean update(Integer courseId, Integer id, CourseCommentDto courseCommentDto) {
        User user = userService.getCurrentUser();
        Wrapper<CourseComment> wrapper = new LambdaQueryWrapper<CourseComment>().eq(CourseComment::getId, id)
                .eq(CourseComment::getUserId, user.getId());
        CourseComment toUpdate = getOne(wrapper);
        if (ObjectUtil.isNull(toUpdate)) {
            return false;
        }
        Integer oldMark = toUpdate.getMark();
        BeanUtil.copyProperties(courseCommentDto, toUpdate);
        updateById(toUpdate);
        courseService.updateMark(courseId, oldMark, toUpdate.getMark());
        return true;
    }

    @Override
    public List<CourseComment> list(Integer courseId) {
        Wrapper<CourseComment> wrapper = new LambdaQueryWrapper<CourseComment>().eq(CourseComment::getCourseId, courseId);
        List<CourseComment> list = list(wrapper);
        try {
            // 如果用户有评论，将评论移动到列表的第一个
            User user = userService.getCurrentUser();
            list.stream().filter(courseComment -> ObjectUtil.equal(courseComment.getUserId(), user.getId()))
                    .findFirst().ifPresent(courseComment -> {
                        list.remove(courseComment);
                        list.add(0, courseComment);
                    });
        } catch (ApiException ignored) {

        }
        return list;

    }
}
