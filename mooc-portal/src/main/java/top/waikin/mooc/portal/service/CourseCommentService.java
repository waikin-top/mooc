package top.waikin.mooc.portal.service;

import org.springframework.transaction.annotation.Transactional;
import top.waikin.mooc.entity.CourseComment;
import com.baomidou.mybatisplus.extension.service.IService;
import top.waikin.mooc.portal.domain.CourseCommentDto;

import java.util.List;

/**
 * 课程评论 服务类
 */
public interface CourseCommentService extends IService<CourseComment> {

    @Transactional
    int create(Integer courseId, CourseCommentDto courseCommentDto);

    @Transactional
    boolean delete(Integer courseId, Integer id);

    @Transactional
    boolean update(Integer courseId, Integer id, CourseCommentDto courseCommentDto);

    List<CourseComment> list(Integer courseId);
}
