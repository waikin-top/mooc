package top.waikin.mooc.portal.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.waikin.mooc.common.exception.Asserts;
import top.waikin.mooc.entity.Course;
import top.waikin.mooc.entity.User;
import top.waikin.mooc.entity.UserCourseRelation;
import top.waikin.mooc.mapper.UserCourseRelationMapper;
import top.waikin.mooc.portal.domain.UserCourseRelationDto;
import top.waikin.mooc.portal.service.CourseService;
import top.waikin.mooc.portal.service.RankCacheService;
import top.waikin.mooc.portal.service.UserCourseRelationService;
import top.waikin.mooc.portal.service.UserService;

/**
 * 用户和课程关系（选课） 服务实现类
 */
@Service
public class UserCourseRelationServiceImpl extends ServiceImpl<UserCourseRelationMapper, UserCourseRelation> implements UserCourseRelationService {

    @Autowired
    UserService userService;

    @Autowired
    CourseService courseService;

    @Autowired
    RankCacheService rankCacheService;

    @Override
    public boolean join(UserCourseRelationDto userCourseRelationDto) {
        User currentUser = userService.getCurrentUser();
        // 查询课程是否存在
        if (ObjectUtil.isNull(courseService.getById(userCourseRelationDto.getCourseId()))) {
            Asserts.fail("课程不存在");
        }
        // 查询是否已经加入该课程
        Wrapper<UserCourseRelation> wrapper = new LambdaQueryWrapper<UserCourseRelation>().eq(UserCourseRelation::getUserId, currentUser.getId())
                .eq(UserCourseRelation::getCourseId, userCourseRelationDto.getCourseId());
        if (ObjectUtil.isNotNull(getOne(wrapper))) {
            Asserts.fail("已经加入该课程");
        }

        UserCourseRelation userCourseRelation = new UserCourseRelation();
        BeanUtil.copyProperties(userCourseRelationDto, userCourseRelation);
        userCourseRelation.setUserId(currentUser.getId());
        save(userCourseRelation);
        // 增加课程人数
        courseService.increaseEnrollCount(userCourseRelation.getCourseId());
        // 增加课程排行热度
        rankCacheService.increaseCourseRank(userCourseRelation.getCourseId());
        return true;
    }

    @Override
    public boolean drop(Integer courseId) {
        Wrapper<UserCourseRelation> wrapper = new LambdaQueryWrapper<UserCourseRelation>().eq(UserCourseRelation::getCourseId, courseId)
                .eq(UserCourseRelation::getUserId, userService.getCurrentUser().getId());
        boolean b = remove(wrapper);
        if (b) {
            // 减少课程人数
            courseService.decreaseEnrollCount(courseId);
        }
        return b;
    }

    @Override
    public IPage<Course> page(Integer current, Integer size, String name) {
        return baseMapper.selectPageByUserIdAndName(new Page<>(current, size), userService.getCurrentUser().getId(), name);
    }
}
