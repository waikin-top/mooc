package top.waikin.mooc.portal.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.waikin.mooc.common.api.CommonResult;
import top.waikin.mooc.entity.CourseData;
import top.waikin.mooc.portal.domain.CourseDataDto;
import top.waikin.mooc.portal.service.CourseDataService;

import java.util.List;

/**
 * 教学课程资料 控制器
 */
@RestController
@Api(tags = "TeachDataController", description = "教学课程资料控制器")
@RequestMapping("/teach/{courseId}/data")
public class TeachDataController {
    @Autowired
    CourseDataService courseDataService;

    @ApiOperation("创建课程资料")
    @PostMapping
    public CommonResult<Integer> create(@PathVariable Integer courseId,
                                        @RequestBody @Validated CourseDataDto courseDataDto) {
        int id = courseDataService.create(courseId, courseDataDto);
        return CommonResult.success(id);
    }

    @ApiOperation("删除课程资料")
    @DeleteMapping("/{id}")
    public CommonResult delete(@PathVariable Integer courseId,
                               @PathVariable Integer id) {
        boolean b = courseDataService.delete(courseId, id);
        return CommonResult.decide(b);
    }

    @ApiOperation("修改课程资料")
    @PutMapping("/{id}")
    public CommonResult update(@PathVariable Integer courseId,
                               @PathVariable Integer id,
                               @RequestBody @Validated CourseDataDto courseDataDto) {
        boolean b = courseDataService.update(courseId, id, courseDataDto);
        return CommonResult.decide(b);
    }

    @ApiOperation("获取课程资料列表")
    @GetMapping
    public CommonResult<List<CourseData>> list(@PathVariable Integer courseId) {
        List<CourseData> dataList = courseDataService.list(courseId);
        return CommonResult.success(dataList);
    }
}
