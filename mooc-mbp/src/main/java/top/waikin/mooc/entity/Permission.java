package top.waikin.mooc.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * 后台权限
 */
@Getter
@Setter
@Accessors(chain = true)
@ApiModel(value = "Permission对象", description = "后台权限")
public class Permission implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("资源名称")
    private String name;

    @ApiModelProperty("路径 /admin/**")
    private String url;

    @ApiModelProperty("请求方法 GET,POST,PUT,DELETE,ALL")
    private String method;

    @ApiModelProperty("描述")
    private String description;


}
