package top.waikin.mooc.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * 频道
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("course_channel")
@ApiModel(value = "CourseChannel对象", description = "频道")
public class CourseChannel implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("频道名称")
    private String name;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("公开课程数量")
    private Integer courseCount;


}
