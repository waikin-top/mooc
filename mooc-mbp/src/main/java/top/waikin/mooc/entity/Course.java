package top.waikin.mooc.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * 课程
 */
@Getter
@Setter
@Accessors(chain = true)
@ApiModel(value = "Course对象", description = "课程")
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("用户id")
    private Integer userId;

    @ApiModelProperty("频道id")
    private Integer channelId;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("课程名称")
    private String name;

    @ApiModelProperty("课程描述")
    private String description;

    @ApiModelProperty("课程概述")
    private String summary;

    @ApiModelProperty("加入课程人数")
    private Integer enrollCount;

    @ApiModelProperty("课程图片")
    private String pic;

    @ApiModelProperty("公开状态 0->不公开,1->公开")
    private Integer isPublic;

    @ApiModelProperty("评分人数")
    private Integer markCount;

    @ApiModelProperty("评分平均分")
    private Float averageMark;


}
