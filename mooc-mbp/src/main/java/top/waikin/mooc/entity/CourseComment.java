package top.waikin.mooc.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * 课程评论
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("course_comment")
@ApiModel(value = "CourseComment对象", description = "课程评论")
public class CourseComment implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("用户id")
    private Integer userId;

    @ApiModelProperty("课程id")
    private Integer courseId;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("评论内容")
    private String content;

    @ApiModelProperty("评论分数 0-5分")
    private Integer mark;


}
