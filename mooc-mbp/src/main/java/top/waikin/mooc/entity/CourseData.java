package top.waikin.mooc.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * 课程资料
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("course_data")
@ApiModel(value = "CourseData对象", description = "课程资料")
public class CourseData implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("课程id")
    private Integer courseId;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty("文件名称")
    private String name;

    @ApiModelProperty("扩展名")
    private String extension;

    @ApiModelProperty("路径")
    private String url;

    @ApiModelProperty("排序")
    private Integer sort;


}
