package top.waikin.mooc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * 管理员和角色关系
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("admin_role_relation")
@ApiModel(value = "AdminRoleRelation对象", description = "管理员和角色关系")
public class AdminRoleRelation implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("管理员id")
    private Integer adminId;

    @ApiModelProperty("角色id")
    private Integer roleId;


}
