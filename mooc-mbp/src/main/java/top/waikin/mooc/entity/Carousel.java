package top.waikin.mooc.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class Carousel {

    @ApiModelProperty("轮播图标题")
    private String title;

    @ApiModelProperty("轮播图图片链接")
    private String pic;

    @ApiModelProperty("轮播图跳转链接")
    private String url;
}
