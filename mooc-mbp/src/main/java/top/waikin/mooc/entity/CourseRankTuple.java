package top.waikin.mooc.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "CourseRankTuple对象", description = "课程热度对象")
public class CourseRankTuple {

    @ApiModelProperty("热度")
    private Double score;

    @ApiModelProperty("课程")
    private Course course;
}
