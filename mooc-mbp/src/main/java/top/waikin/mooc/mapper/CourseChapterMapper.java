package top.waikin.mooc.mapper;

import top.waikin.mooc.entity.CourseChapter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 课程章节 Mapper 接口
 */
public interface CourseChapterMapper extends BaseMapper<CourseChapter> {

}
