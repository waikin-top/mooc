package top.waikin.mooc.mapper;

import top.waikin.mooc.entity.Course;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 课程 Mapper 接口
 */
public interface CourseMapper extends BaseMapper<Course> {

}
