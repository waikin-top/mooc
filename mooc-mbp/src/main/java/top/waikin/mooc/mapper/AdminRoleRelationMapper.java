package top.waikin.mooc.mapper;

import top.waikin.mooc.entity.AdminRoleRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.waikin.mooc.entity.Permission;
import top.waikin.mooc.entity.Role;

import java.util.List;

/**
 * 管理员和角色关系 Mapper 接口
 */
public interface AdminRoleRelationMapper extends BaseMapper<AdminRoleRelation> {

    List<Role> getRoleListByAdminId(Integer id);

    List<Permission> getPermissionListByAdminId(Integer adminId);
}
