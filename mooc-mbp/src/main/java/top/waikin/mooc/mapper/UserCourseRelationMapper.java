package top.waikin.mooc.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import top.waikin.mooc.entity.Course;
import top.waikin.mooc.entity.UserCourseRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户和课程关系（选课） Mapper 接口
 */
public interface UserCourseRelationMapper extends BaseMapper<UserCourseRelation> {

    IPage<Course> selectPageByUserIdAndName(IPage<Course> coursePage, Integer userId, String name);
}
