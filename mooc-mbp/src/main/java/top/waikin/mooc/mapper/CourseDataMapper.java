package top.waikin.mooc.mapper;

import top.waikin.mooc.entity.CourseData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 课程资料 Mapper 接口
 */
public interface CourseDataMapper extends BaseMapper<CourseData> {

}
