package top.waikin.mooc.mapper;

import top.waikin.mooc.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户 Mapper 接口
 */
public interface UserMapper extends BaseMapper<User> {

}
