package top.waikin.mooc.mapper;

import top.waikin.mooc.entity.Admin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 后台 管理员 Mapper 接口
 */
public interface AdminMapper extends BaseMapper<Admin> {

}
