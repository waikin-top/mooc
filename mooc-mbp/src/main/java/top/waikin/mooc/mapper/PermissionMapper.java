package top.waikin.mooc.mapper;

import top.waikin.mooc.entity.Permission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 后台权限 Mapper 接口
 */
public interface PermissionMapper extends BaseMapper<Permission> {

}
