package top.waikin.mooc.mapper;

import top.waikin.mooc.entity.CourseChannel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 频道 Mapper 接口
 */
public interface CourseChannelMapper extends BaseMapper<CourseChannel> {

}
