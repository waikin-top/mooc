package top.waikin.mooc.mapper;

import top.waikin.mooc.entity.CourseComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 课程评论 Mapper 接口
 */
public interface CourseCommentMapper extends BaseMapper<CourseComment> {

}
