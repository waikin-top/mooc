package top.waikin.mooc.mapper;

import top.waikin.mooc.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 后台用户角色 Mapper 接口
 */
public interface RoleMapper extends BaseMapper<Role> {

}
