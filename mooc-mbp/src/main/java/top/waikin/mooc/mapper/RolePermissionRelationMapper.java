package top.waikin.mooc.mapper;

import top.waikin.mooc.entity.RolePermissionRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * 后台 角色和资源关系 Mapper 接口
 */
public interface RolePermissionRelationMapper extends BaseMapper<RolePermissionRelation> {

    List<Integer> getAdminIdListByPermissionId(Integer permissionId);
}
