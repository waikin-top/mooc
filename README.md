# mooc


## 项目介绍
`mooc`项目是一套在线教育系统，包括前台用户系统及后台管理系统，基于SpringBoot+MyBatis-Plus实现。前台用户系统包含首页门户、课程推荐、课程排行、课程搜索、课程展示、教学课程、学习课程、课程评价的模块。后台管理系统包含门户管理、推荐管理、频道管理、课程管理、排行分析、权限管理等模块。

## 项目接口文档

- 后台：[https://mooc-admin.waikin.top](https://mooc-admin.waikin.top/doc.html)
- 前台：[https://mooc-portal.waikin.top](https://mooc-portal.waikin.top/doc.html)

### 组织结构

``` lua
mooc
├── mooc-common -- 工具类及通用代码
├── mooc-mbp -- MyBatisPlusGenerator生成的数据库操作代码
├── mooc-security -- Sa-Token封装公用模块
├── mooc-admin -- 后台管理系统接口
└── mooc-portal -- 前台系统接口
```

### 技术选型

| 技术                   | 说明          | 官网                                             |
|----------------------|-------------|------------------------------------------------|
| SpringBoot           | 容器+MVC框架    | https://spring.io/projects/spring-boot         |
| Sa-Token             | 认证和授权框架     | https://sa-token.dev33.cn/                     |
| MyBatis              | ORM框架       | http://www.mybatis.org/mybatis-3/zh/index.html |
| MyBatisPlus          | MyBatis增强工具 | https://baomidou.com/                          |
| Redis                | 缓存          | https://redis.io/                              |
| HikariCP             | 数据库连接池      | https://github.com/brettwooldridge/HikariCP    |
| COS                  | 对象存储        | https://github.com/tencentyun/cos-java-sdk-v5  |
| 腾讯云短信                | 短信服务        | https://cloud.tencent.com/                     |
| JWT                  | JWT登录支持     | https://github.com/jwtk/jjwt                   |
| Lombok               | 简化对象封装工具    | https://github.com/rzwitserloot/lombok         |
| Hutool               | Java工具类库    | https://github.com/looly/hutool                |
| Swagger-UI           | 文档生成工具      | https://github.com/swagger-api/swagger-ui      |
| Hibernator-Validator | 验证框架        | http://hibernate.org/validator                 |

### 开发工具

| 工具           | 说明           | 官网                                                    |
|--------------|--------------|-------------------------------------------------------|
| IDEA         | 开发IDE        | https://www.jetbrains.com/idea/download               |
| RedisDesktop | redis客户端连接工具 | https://github.com/qishibo/AnotherRedisDesktopManager |
| SwitchHosts  | 本地host管理     | https://oldj.github.io/SwitchHosts/                   |
| X-shell      | Linux远程连接工具  | http://www.netsarang.com/download/software.html       |
| Navicat      | 数据库连接和设计工具   | http://www.formysql.com/xiazai.html                   |
| XMind        | 思维导图设计工具     | http://www.edrawsoft.cn/mindmaster                    |
| ProcessOn    | 流程图绘制工具      | https://www.processon.com/                            |
| FastRequest  | API接口调试工具    | https://dromara.gitee.io/fast-request/                |

### 开发环境

| 工具          | 版本号 | 下载                                                         |
| ------------- | ------ | ------------------------------------------------------------ |
| JDK           | 1.8    | https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html |
| Mysql         | 5.7    | https://www.mysql.com/                                       |
| Redis         | 7.0    | https://redis.io/download                                    |


