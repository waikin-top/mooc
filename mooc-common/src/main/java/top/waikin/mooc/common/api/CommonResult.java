package top.waikin.mooc.common.api;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 通用返回对象
 */
@Data
@AllArgsConstructor
public class CommonResult<T> {
    private long code;

    private String message;

    private T data;

    public static <T> CommonResult<T> success(T data) {
        return new CommonResult<>(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMessage(), data);
    }

    public static <T> CommonResult<T> success(T data, String message) {
        return new CommonResult<>(ResultCode.SUCCESS.getCode(), message, data);
    }

    public static <T> CommonResult<T> failed(ResultCode resultCode) {
        return new CommonResult<>(resultCode.getCode(), resultCode.getMessage(), null);
    }

    public static <T> CommonResult<T> failed(String message) {
        return new CommonResult<>(ResultCode.FAILED.getCode(), message, null);
    }

    public static <T> CommonResult<T> failed() {
        return failed(ResultCode.FAILED);
    }

    public static CommonResult decide(boolean b) {
        if (b) {
            return CommonResult.success(null);
        }
        return CommonResult.failed();
    }
}
