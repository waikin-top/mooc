package top.waikin.mooc.common.service;

/**
 * SMS
 */
public interface SMSService {
    /**
     * 发送验证码
     */
    void sendAuthCode(String telephone,String authCode);
}
