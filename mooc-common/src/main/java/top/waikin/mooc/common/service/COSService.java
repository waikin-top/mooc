package top.waikin.mooc.common.service;

import java.net.URL;

/**
 * COS
 */
public interface COSService {
    /**
     * 生成上传链接
     */
    URL generateUploadUrl();
}
