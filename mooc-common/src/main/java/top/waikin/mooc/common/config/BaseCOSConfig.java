package top.waikin.mooc.common.config;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.region.Region;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import top.waikin.mooc.common.service.COSService;
import top.waikin.mooc.common.service.impl.COSServiceImpl;

/**
 * 腾讯云COS配置类
 */
public class BaseCOSConfig {
    @Value("${tencent-cloud.cos.secretId}")
    private String SECRET_ID;
    @Value("${tencent-cloud.cos.secretKey}")
    private String SECRET_KEY;
    @Value("${tencent-cloud.cos.region}")
    private String REGION;

    @Bean
    COSClient cosClient() {
        COSCredentials cred = new BasicCOSCredentials(SECRET_ID, SECRET_KEY);
        Region region = new Region(REGION);
        ClientConfig clientConfig = new ClientConfig(region);
        return new COSClient(cred, clientConfig);
    }

    @Bean
    COSService COSService() {
        return new COSServiceImpl();
    }
}
