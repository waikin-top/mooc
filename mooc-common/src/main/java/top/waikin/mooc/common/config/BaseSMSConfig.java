package top.waikin.mooc.common.config;

import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.sms.v20210111.SmsClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import top.waikin.mooc.common.service.SMSService;
import top.waikin.mooc.common.service.impl.SMSServiceImpl;

/**
 * 短信配置类
 */
public class BaseSMSConfig {
    @Value("${tencent-cloud.sms.secretId}")
    private String SECRET_ID;
    @Value("${tencent-cloud.sms.secretKey}")
    private String SECRET_KEY;
    @Value("${tencent-cloud.sms.region}")
    private String REGION;

    @Bean
    SmsClient smsClient() {
        Credential credential = new Credential(SECRET_ID, SECRET_KEY);
        ClientProfile clientProfile = new ClientProfile();
        return new SmsClient(credential, REGION, clientProfile);
    }

    @Bean
    SMSService smsService() {
        return new SMSServiceImpl();
    }
}
