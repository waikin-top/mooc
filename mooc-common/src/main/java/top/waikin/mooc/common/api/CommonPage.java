package top.waikin.mooc.common.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 通用分页对象
 */
@Data
@Accessors(chain = true)
public class CommonPage<T> {
    /**
     * 当前页码
     */
    private Long currentPage;
    /**
     * 每页数量
     */
    private Long pageSize;
    /**
     * 总页数
     */
    private Long totalPage;
    /**
     * 总条数
     */
    private Long total;
    /**
     * 分页数据
     */
    private List<T> records;

    public static <T> CommonPage<T> restPage(IPage<T> page) {
        return new CommonPage<T>()
                .setCurrentPage(page.getCurrent())
                .setPageSize(page.getSize())
                .setTotalPage(page.getTotal())
                .setTotal(page.getTotal())
                .setRecords(page.getRecords());
    }
}
