package top.waikin.mooc.common.exception;


import lombok.Getter;
import top.waikin.mooc.common.api.ResultCode;

/**
 * 自定义异常
 */
@Getter
public class ApiException extends RuntimeException {

    private ResultCode resultCode;

    public ApiException(ResultCode resultCode) {
        super(resultCode.getMessage());
        this.resultCode = resultCode;
    }

    public ApiException(String message) {
        super(message);
    }
}
