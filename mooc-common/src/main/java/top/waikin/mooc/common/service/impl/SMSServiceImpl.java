package top.waikin.mooc.common.service.impl;

import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.sms.v20210111.SmsClient;
import com.tencentcloudapi.sms.v20210111.models.SendSmsRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import top.waikin.mooc.common.exception.Asserts;
import top.waikin.mooc.common.service.SMSService;


/**
 * SMS实现
 */
public class SMSServiceImpl implements SMSService {

    @Value("${tencent-cloud.sms.sdkAppId}")
    private String SDK_APP_ID;
    @Value("${tencent-cloud.sms.signName}")
    private String SIGN_NAME;
    @Value("${tencent-cloud.sms.templateId}")
    private String TEMPLATE_ID;

    @Autowired
    SmsClient smsClient;

    @Override
    public void sendAuthCode(String telephone, String authCode) {
        SendSmsRequest sendSmsRequest = new SendSmsRequest();
        sendSmsRequest.setSmsSdkAppId(SDK_APP_ID);
        sendSmsRequest.setSignName(SIGN_NAME);
        sendSmsRequest.setTemplateId(TEMPLATE_ID);
        sendSmsRequest.setPhoneNumberSet(new String[]{telephone});
        sendSmsRequest.setTemplateParamSet(new String[]{authCode});
        try {
            smsClient.SendSms(sendSmsRequest);
        } catch (TencentCloudSDKException e) {
            Asserts.fail(e.getMessage());
        }
    }
}
