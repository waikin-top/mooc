SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `telephone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `profile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `note` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `status` int(11) NULL DEFAULT NULL COMMENT '帐号启用状态:0->禁用,1->启用',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_telephone`(`telephone`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '后台 管理员' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 'admin', 'admin', '超级管理员', 'https://mooc-1258408218.cos.ap-guangzhou.myqcloud.com/profile.png', '超管 有所有权限', NULL, 1);

-- ----------------------------
-- Table structure for admin_role_relation
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_relation`;
CREATE TABLE `admin_role_relation`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NULL DEFAULT NULL COMMENT '管理员id',
  `role_id` int(11) NULL DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员和角色关系' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admin_role_relation
-- ----------------------------
INSERT INTO `admin_role_relation` VALUES (1, 1, 1);

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `channel_id` int(11) NULL DEFAULT NULL COMMENT '频道id',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '课程名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '课程描述',
  `summary` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '课程概述',
  `enroll_count` int(11) NULL DEFAULT NULL COMMENT '加入课程人数',
  `pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '课程图片',
  `is_public` int(11) NULL DEFAULT NULL COMMENT '公开状态 0->不公开,1->公开',
  `mark_count` int(11) NULL DEFAULT NULL COMMENT '评分人数',
  `average_mark` float NULL DEFAULT NULL COMMENT '评分平均分',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '课程' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES (2, NULL, 1, NULL, 'name_bixrt', 'description_a1g3u', 'summary_mjr34', 0, 'http://pic_im4p7', 1, 0, 0);
INSERT INTO `course` VALUES (3, 1, 1, NULL, 'abab', 'description_x5svg', 'summary_tvg4u', 2, 'http://pic_1r2vs', 0, 0, 0);
INSERT INTO `course` VALUES (5, 1, 1, '2022-11-02 14:47:26', '王者', 'description_ez760', 'summary_skvl7', 2, 'http://pic_ji99n', 1, 3, 4.66667);

-- ----------------------------
-- Table structure for course_channel
-- ----------------------------
DROP TABLE IF EXISTS `course_channel`;
CREATE TABLE `course_channel`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '频道名称',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `course_count` int(11) NOT NULL COMMENT '公开课程数量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '频道' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of course_channel
-- ----------------------------
INSERT INTO `course_channel` VALUES (1, '计算机', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (2, '大数据与人工智能', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (3, '软件工程', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (4, '外语', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (5, '理学工学农学', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (6, '数学', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (7, '物理', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (8, '化学', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (9, '天文学', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (10, '地理科学', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (11, '生物科学', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (12, '电气信息', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (13, '大气与海洋', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (14, '机械', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (15, '土建水利', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (16, '力学', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (17, '材料', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (18, '交通运输', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (19, '化工与生物制药', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (20, '能源矿业', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (21, '轻纺食品', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (22, '航空航天', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (23, '农林环境', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (24, '安全', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (25, '植物', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (26, '动物', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (27, '生态', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (28, '考研', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (29, '数学', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (30, '英语', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (31, '政治', NULL, NULL, 0);
INSERT INTO `course_channel` VALUES (32, '四六级', NULL, NULL, 0);

-- ----------------------------
-- Table structure for course_chapter
-- ----------------------------
DROP TABLE IF EXISTS `course_chapter`;
CREATE TABLE `course_chapter`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `next_id` int(11) NULL DEFAULT NULL COMMENT '下一章节id 没有下一章节为-1',
  `course_id` int(11) NULL DEFAULT NULL COMMENT '课程id',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '章节名称',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '章节内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 96 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '课程章节' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of course_chapter
-- ----------------------------
INSERT INTO `course_chapter` VALUES (90, 91, 5, '2022-11-04 00:45:48', 'name_nfcmj', 'content_bi5gg');
INSERT INTO `course_chapter` VALUES (91, 93, 5, '2022-11-04 00:45:49', 'name_nfcmj', 'content_bi5gg');
INSERT INTO `course_chapter` VALUES (93, 94, 5, '2022-11-04 00:45:51', 'name_nfcmj', 'content_bi5gg');
INSERT INTO `course_chapter` VALUES (94, -1, 5, '2022-11-04 00:50:45', 'name_nfcmj', 'content_bi5gg');
INSERT INTO `course_chapter` VALUES (95, 90, 5, '2022-11-04 00:51:04', 'name_nfcmj', 'content_bi5gg');

-- ----------------------------
-- Table structure for course_comment
-- ----------------------------
DROP TABLE IF EXISTS `course_comment`;
CREATE TABLE `course_comment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `course_id` int(11) NULL DEFAULT NULL COMMENT '课程id',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评论内容',
  `mark` int(11) NULL DEFAULT NULL COMMENT '评论分数 0-5分',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '课程评论' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of course_comment
-- ----------------------------
INSERT INTO `course_comment` VALUES (1, 1, 5, '2022-11-04 15:06:39', 'content_4l5cy', 4);
INSERT INTO `course_comment` VALUES (8, 2, 5, '2022-11-04 20:50:43', 'content_rhdf1', 5);
INSERT INTO `course_comment` VALUES (9, 3, 5, '2022-11-04 22:55:33', 'content_rhdf1', 5);

-- ----------------------------
-- Table structure for course_data
-- ----------------------------
DROP TABLE IF EXISTS `course_data`;
CREATE TABLE `course_data`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NULL DEFAULT NULL COMMENT '课程id',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件名称',
  `extension` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展名',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路径',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '课程资料' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of course_data
-- ----------------------------
INSERT INTO `course_data` VALUES (3, 5, '2022-11-03 14:42:29', 'name_c0nua', 'extension_5pdom', 'http://url_9rg8b', 1);
INSERT INTO `course_data` VALUES (4, 5, '2022-11-03 14:42:40', 'name_c0nua', 'extension_5pdom', 'http://url_9rg8b', 1);
INSERT INTO `course_data` VALUES (5, 5, '2022-11-03 14:42:45', 'name_c0nua', 'extension_5pdom', 'http://url_9rg8b', 1);
INSERT INTO `course_data` VALUES (6, 5, '2022-11-03 14:49:14', 'name_c0nua', 'extension_5pdom', 'http://url_9rg8b', 1);
INSERT INTO `course_data` VALUES (7, 5, '2022-11-03 15:10:53', 'name_c0nua', 'extension_5pdom', 'http://url_9rg8b', 1);

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源名称',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路径 /admin/**',
  `method` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求方法 GET,POST,PUT,DELETE',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '后台权限' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES (1, NULL, '所有权限', '/**', 'ALL', NULL);
INSERT INTO `permission` VALUES (2, NULL, '修改管理员', '/admin', 'PUT', NULL);
INSERT INTO `permission` VALUES (3, NULL, '获取指定管理员', '/admin/*', 'GET', NULL);
INSERT INTO `permission` VALUES (4, NULL, '获取管理员列表', '/admin', 'GET', NULL);
INSERT INTO `permission` VALUES (5, NULL, '给管理员分配角色', '/admin/*/role', 'POST', NULL);
INSERT INTO `permission` VALUES (6, NULL, '获取管理员角色列表', '/admin/*/role', 'GET', NULL);
INSERT INTO `permission` VALUES (7, NULL, '创建权限', '/permission', 'POST', NULL);
INSERT INTO `permission` VALUES (8, NULL, '删除权限', '/permission/*', 'DELETE', NULL);
INSERT INTO `permission` VALUES (9, NULL, '修改权限', '/permission/*', 'PUT', NULL);
INSERT INTO `permission` VALUES (10, NULL, '获取权限列表', '/permission', 'GET', NULL);
INSERT INTO `permission` VALUES (11, NULL, '创建角色', '/role', 'POST', NULL);
INSERT INTO `permission` VALUES (12, NULL, '删除角色', '/role/*', 'DELETE', NULL);
INSERT INTO `permission` VALUES (13, NULL, '修改角色', '/role/*', 'PUT', NULL);
INSERT INTO `permission` VALUES (14, NULL, '获取角色列表', '/role', 'GET', NULL);
INSERT INTO `permission` VALUES (15, NULL, '给角色分配权限', '/role/*/permission', 'POST', NULL);
INSERT INTO `permission` VALUES (16, NULL, '创建频道', '/channel', 'POST', NULL);
INSERT INTO `permission` VALUES (17, NULL, '删除频道', '/channel/*', 'DELETE', NULL);
INSERT INTO `permission` VALUES (18, NULL, '修改频道', '/channel/*', 'PUT', NULL);
INSERT INTO `permission` VALUES (19, NULL, '获取频道列表', '/channel', 'GET', NULL);
INSERT INTO `permission` VALUES (20, NULL, '删除课程', '/course/*', 'DELETE', NULL);
INSERT INTO `permission` VALUES (21, NULL, '获取指定课程', '/course/*', 'GET', NULL);
INSERT INTO `permission` VALUES (22, NULL, '分页查询课程', '/course', 'GET', NULL);
INSERT INTO `permission` VALUES (23, NULL, '添加推荐课程', '/home/recommend/*', 'POST', NULL);
INSERT INTO `permission` VALUES (24, NULL, '删除课程推荐', '/home/recommend/*', 'DELETE', NULL);
INSERT INTO `permission` VALUES (25, NULL, '获取课程推荐列表', '/home/recommend', 'GET', NULL);
INSERT INTO `permission` VALUES (26, NULL, '创建轮播图', '/home/carousel', 'POST', NULL);
INSERT INTO `permission` VALUES (27, NULL, '删除轮播图', '/home/carousel/*', 'DELETE', NULL);
INSERT INTO `permission` VALUES (28, NULL, '更新轮播图', '/home/carousel/*', 'PUT', NULL);
INSERT INTO `permission` VALUES (29, NULL, '获取轮播图列表', '/home/carousel', 'GET', NULL);
INSERT INTO `permission` VALUES (30, NULL, '获取排行列表', '/rank/**', 'GET', NULL);

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `status` int(11) NULL DEFAULT NULL COMMENT '启用状态:0->禁用,1->启用',
  `admin_count` int(11) NULL DEFAULT NULL COMMENT '后台用户数量 实时更新',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '后台用户角色' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, NULL, '超级管理员', '拥有所有权限', 1, 1, 1);

-- ----------------------------
-- Table structure for role_permission_relation
-- ----------------------------
DROP TABLE IF EXISTS `role_permission_relation`;
CREATE TABLE `role_permission_relation`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NULL DEFAULT NULL COMMENT '角色id',
  `permission_id` int(11) NULL DEFAULT NULL COMMENT '权限id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 69 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '后台 角色和资源关系' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of role_permission_relation
-- ----------------------------
INSERT INTO `role_permission_relation` VALUES (68, 1, 1);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `telephone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `gender` int(11) NULL DEFAULT NULL COMMENT '性别:0->未知,1->男,2->女',
  `profile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `status` int(11) NULL DEFAULT NULL COMMENT '帐号启用状态:0->禁用,1->启用',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unique_telephone`(`telephone`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'user', 'user', NULL, NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for user_course_relation
-- ----------------------------
DROP TABLE IF EXISTS `user_course_relation`;
CREATE TABLE `user_course_relation`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `course_id` int(11) NULL DEFAULT NULL COMMENT '课程id',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户和课程关系（选课）' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_course_relation
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
